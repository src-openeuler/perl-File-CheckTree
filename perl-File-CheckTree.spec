Name:                perl-File-CheckTree
Version:             4.42
Release:             305
Summary:             On a tree to run many file-test checks
License:             GPL-1.0-or-later OR Artistic-1.0-Perl
URL:                 https://metacpan.org/release/File-CheckTree
Source0:             https://cpan.metacpan.org/authors/id/R/RJ/RJBS/File-CheckTree-%{version}.tar.gz

BuildArch:           noarch

BuildRequires:       make perl-generators perl-interpreter perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:       perl(strict) perl(warnings) perl(Cwd) perl(deprecate) perl(Exporter)
BuildRequires:       perl(File::Spec) perl(if) perl(overload) perl(Test::More)

Requires:            perl(deprecate)

%description
The package is to run many file-test checks on a tree.
File::CheckTree::validate() routine takes a single multi-line string consisting of directives,
each containing a file name plus a file test to try on it. After the file test you may put ||
die to make it a fatal error if the file test fails. The default is || warn.

%package help
Summary:              Help documents for perl File-CheckTree

%description help
Help documents for perl File-CheckTree.

%prep
%autosetup -n File-CheckTree-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
unset RELEASE_TESTING
make test

%files
%license LICENSE
%{perl_vendorlib}/*

%files help
%license LICENSE
%doc Changes README
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 4.42-305
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Apr 23 2020 leiju <leiju4@huawei.com> - 4.42-304
- Package init
